package org.gcube.portlets.user.td.gwtservice.shared.function.aggregate;

/**
 * 
 * @author Giancarlo Panichi
 *
 * 
 */
public class TdAggregateFunction extends TdBaseComboDataBean {

	private static final long serialVersionUID = 2341132987652197369L;

	public TdAggregateFunction() {
	}

	public TdAggregateFunction(String id, String label) {
		super(id, label);
	}

}
